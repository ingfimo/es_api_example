from datetime import datetime
from elasticsearch import Elasticsearch
from fastapi import FastAPI, HTTPException, Response
from pydantic import BaseModel
from json import load as jsonLoad


def searchResponse(resp: dict, response: Response) -> dict:
    """
    Decorator for the response of a search.
    It change the status code from 200 to 204 in the case that the
    search result list is empty.
    
    Args:
        resp (dct): search response
        response (Response): FastAPI Response object for recording 'status_code' changes
    """
    if len(resp["hits"]["hits"]) < 1:
        response.status_code = 204
    return resp


class Query(BaseModel):
    """
    Data model for simple search queries.
    The matching documents are returned in reverse chronological order
    (i.e. from the newest to the oldest)

    Attributes:
        pattern (str): pattern to search for
        k (int): number of documents to return starting from the top of the result list
    """

    pattern: str = None
    k: int = 10

    def checkAttrs(self):
        """
        Checks that the attributes have legal values.
        """
        if self.k < 1:
            raise HTTPException(400, "'k' must be higher than 0.")
        if self.pattern is None:
            raise HTTPException(400, "'pattern' cannot be None.")
        if len(self.pattern) < 1:
            raise HTTPException(400, "'pattern' must contain at least one character.")
        return True

    def qry(self) -> dict:
        """
        Returns the JSON query for the Elasticsearch server.
        """
        return {
            "size": f"{self.k}",
            "sort": {"timestamp": {"order": "desc"}},
            "query": {
                "multi_match": {
                    "query": self.pattern,
                    "fields": ["title", "text", "author"],
                }
            },
        }

    def fuzzyQry(self):
        """
        Returns the fuzzy version of the JSON query for the Elasticsearch server.
        """
        qry = self.qry()
        qry["query"]["multi_match"]["fuzziness"] = "AUTO"
        return qry


class WeightedQuery(Query):
    """
    Data model for a weighted query, that is is allows to set different boosting values
    for the matching of the different document fields.
    Matching documents are returned in decreasing order of the matching score.
    The default matching score is BM25 with default parameters.

    Attributes:
        patterns (str): pattern o search for
        k (int): number of document to return starting form the top of the result list
        boost_title (float): boosting value for the document title
        boost_text (float): boosting value for the document text
        boost_author (float): boosting value for the document author
    """

    boost_title: float = 1
    boost_text: float = 1
    boost_author: float = 1

    def checkAttrs(self):
        """
        Checks the validity of the boosting attributes.
        """
        if super().checkAttrs():
            if self.boost_title < 0:
                raise HTTPException(
                    status_code=400,
                    detail="'boost_title' must be higher then or equal to 0.",
                )
            if self.boost_text < 0:
                raise HTTPException(
                    status_code=400,
                    detail="'boost_text' must be higher then or equal to 0.",
                )
            if self.boost_author < 0:
                raise HTTPException(
                    status_code=400,
                    detail="'boost_author' must be higher then or equal to 0.",
                )
        return True

    def qry(self) -> dict:
        """
        Returns the JSON query for the Elasticsearch server.
        """
        return {
            "size": f"{self.k}",
            "sort": "_score",
            "query": {
                "multi_match": {
                    "query": self.pattern,
                    "fields": [
                        f"title^{self.boost_title}",
                        f"text^{self.boost_text}",
                        f"author^{self.boost_author}",
                    ],
                }
            },
        }


class Document(BaseModel):
    """
    Data model for a document.
    A legal document must have at least a 'text'.
    
    Attributes:
        title (str): document title
        text (str): document text
        author (str): document author
        id (str): document id (only for updating existing documents)
    """

    title: str = None
    text: str = None
    author: str = None
    id: str = None

    def checkAttrs(self):
        """
        Checks that the document is legal.
        """
        if self.text is None:
            raise HTTPException(400, "A document must have a text.")
        return True

    def body(self) -> dict:
        """
        Returns the JSON body of the document object.
        """
        return {
            "title": self.title,
            "author": self.author,
            "text": self.text,
            "timestamp": datetime.now(),
        }


class EsInterface(object):
    """
    Class defining the interface between the endpoint and the Elasticsearch
    server.
    """

    def __init__(self, index: str, index_settings: dict, **kwargs):
        """
        Initialization method.
        
        Args:
            index (str): index name
            index_settings (dict): dict representing the settings and mappings 
                for the Elasticsearch index
            host (str): node (nodes) for connecting to the Elasticsearch server
            **kwargs: additional arguments for the 'Elasticsearch' class
        """
        self.es = Elasticsearch(**kwargs)
        self.index = index
        self.es.indices.create(index=self.index, body=index_settings, ignore=400)

    def getIndexSettings(self) -> dict:
        """
        Returns the index settings.
        """
        return self.es.indices.get_settings(index=self.index)

    def getIndexMappings(self) -> dict:
        """
        Returns the index mappings.
        """
        return self.es.indices.get_mapping(index=self.index)

    def docExists(self, id: str) -> bool:
        """
        Returns True if a document with the given id is found in the index.

        Args:
            id (str): document id
        """
        return self.es.exists(index=self.index, id=id)

    def indexDoc(self, doc: Document) -> dict:
        """
        Adds the given document to the index.

        Args:
            doc (Document): document to add
        """
        if doc.id is not None:
            raise HTTPException(400, "update request (id provided).")
        return self.es.index(index=self.index, body=doc.body(), id=None)

    def updateDoc(self, doc: Document) -> dict:
        """
        Updates an existing document.

        Args:
           doc (Document): document to update
        """
        if doc.id is None:
            raise HTTPException(400, "addition request (id not provided).")
        if not self.docExists(doc.id):
            raise HTTPException(404, f"document wit id {doc.id} not found.")
        return self.es.index(index=self.index, body=doc.body(), id=doc.id)

    def getDoc(self, doc_id: str) -> dict:
        """
        Given an id it tries ti retrieve the corresponding document.
        
        Args:
            doc_id (str): document id
        """
        if not self.docExists(doc_id):
            raise HTTPException(404, "document with id {id} not found.")
        return self.es.get(index=self.index, id=doc_id)

    def getAll(self) -> dict:
        """
        Retrieves all the documents in the index.
        """
        return self.es.search(
            index=self.index,
            body={"sort": {"timestamp": {"order": "desc"}}, "query": {"match_all": {}}},
        )

    def search(self, qry: dict) -> dict:
        """
        Retrieve the document's matching the given query.

        Args:
            qry (dict): JSON query for the Elasticsearch server
        """
        return self.es.search(index=self.index, body=qry)


def loadSettings():
    """
    Loads the index settings and mappings from the 
    file 'settings.json' that must be loacated in the working directory.
    """
    inf = open("settings.json")
    settings = jsonLoad(inf)
    inf.close()
    return settings


ES = EsInterface("test-index", loadSettings())
app = FastAPI()


@app.get("/settings", status_code=200)
async def getIndexSettings():
    """
    GET end point to retrive the index settings.
    """
    return ES.getIndexSettings()


@app.get("/mappings", status_code=200)
async def getIndexMappings():
    """
    GET endpoint to retrieve the index mappings.
    """
    return ES.getIndexMappings()


@app.get("/retrieve/all", status_code=200)
async def getAll(response: Response):
    """
    GET endpoint to retrieve all the document in the index.
    """
    return searchResponse(ES.getAll(), response)


@app.get("/retrieve/id/{doc_id}", status_code=200)
async def getDoc(doc_id):
    """
    GET endpoint to retrieve a document given its id.
    """
    return ES.getDoc(doc_id)


@app.put("/new", status_code=201)
async def addDoc(doc: Document):
    """
    PUT endpoint to add a new document to the index.
    """
    if doc.checkAttrs():
        return ES.indexDoc(doc)


@app.put("/update", status_code=202)
async def updateDoc(doc: Document):
    """
    PUT endpoint to update an existing document.
    """
    if doc.checkAttrs():
        return ES.updateDoc(doc)


@app.post("/search/simple", status_code=200)
async def simpleSearch(query: Query, response: Response):
    """
    POST endpoint to perform a simple search.
    A simple search returns the top k matching document in reverse
    chronological order (i.e. from the newest document to the oldest one)
    """
    if query.checkAttrs():
        return searchResponse(ES.search(query.qry()), response)


@app.post("/search/simple/fuzzy", status_code=200)
async def simpleSearchFuzzy(query: Query, response: Response):
    """
    POST endpoint to perform a simple search with fuzzy matching.
    """
    if query.checkAttrs():
        return searchResponse(ES.search(query.fuzzyQry()), response)


@app.post("/search/weighted", status_code=200)
async def weightedSearch(query: WeightedQuery, response: Response):
    """
    POST endpoint to perform a weighted search.
    A weighted search allows different boosting parameters for the 
    different document fields.
    Returns the matching documents in descending order w.r.t. the matching score.
    """
    if query.checkAttrs():
        return searchResponse(ES.search(query.qry()), response)


@app.post("/search/weighted/fuzzy", status_code=200)
async def weightedSearchFuzzy(query: WeightedQuery, response: Response):
    """
    POST endpoint to perform a weighted search with fuzzy matching.
    """
    if query.checkAttrs():
        return searchResponse(ES.search(query.fuzzyQry()), response)
