# Elastic search REST API example 

The API as been implemented with Elasticsearch and FastAPI, and Python 3.6.9.
In the following it assumed as working the directory containing this file. 
The symbol `$` indicates the bash prompt.

## Package synopsis
* README.md: this file.
* requirements.txt: python dependencies.
* settings.json: Elasticsearch template for initializing the index settings and mappings.
* api.py: Python implementation through `FastAPI` of the required REST API.
* test_api.py: unit test to run with `pytest`. 
* elasticsearch-7.9.2: Elasticsearch version 7.9.2. 
* venv: Python virtual environment (`virtualenv`) with the required dependencies installed.

## 1. Install dependencies

Get Elasticsearch version 7.9.2 with:

`$ curl -L -O https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.9.2-linux-x86_64.tar.gz`

Extract the tar archive with:

`$ tar -xvf elasticsearch-7.9.2-linux-x86_64.tar.gz`

Install the Python dependencies with:

`$ pip install -r requirements.txt`

Alternatively activate the Python virtual environment with:

`$ source venv/bin/activate`

before executing the step highlighted with (venv).
To deactivate the Python virtual environment:

`$ deactivate`

## 2. Run tests

Start the Elastic search server with:

`$ ./elasticsearch-7.9.2/bin/elasticsearch`

In a new terminal window run `pytest` as:

`(venv)$ pytest test_api.py`

Stop the Elastic search server with Ctrl-C from the relative terminal window.
Clean the Elastic search index with:

`$ rm -rf ./elasticsearch-7.9.2/data`

## 4. Run the application

Start the Elastic search server with:

`$ ./elasticsearch-7.9.2/bin/elasticsearch`

In a new terminal window run `pytest` as:

`(venv)$ uvicorn api:app`

The endpoint will be at http://127.0.0.1:8000. 
The following REST API have been implemented.

* GET /settings: returns the setting of Elasticsearch index.
* GET /mappings: returns the mappings of the Elasticsearch index.
* GET /retrieve/all: retrieves all the documents in the index.
* GET /retrieve/id/<doc_id>: retrieves the documents indexed at the give id.
* PUT /new: indexes a new document provided as JSON 
(see the `Document` data model and the `addDoc` function in `api.py`).
* PUT /update: updated an indexed document 
(see the `Document` data model and the `updateDoc` function in `api.py`).
* POST /search/simple: perform a simple search 
(see the `Query` data model and the `simpleSearch` function in `api.py`). 
* POST /search/simple/fuzzy: perform a simple search with fuzzy matching 
(see the `Query` data model and the `simpleSearchFuzzy` function in `api.py`).
* POST /search/weighted: perform a weighted search with different boosting parameters for each document field
(see the `WeightedQuery` data model and the `weightedSearch` function in `api.py`).
* POST /search/weighted/fuzzy: perform a weighted search with fuzzy matching and 
with different boosting parameters for each document field
(see the `WeightedQuery` data model and the `weightedSearch` function in `api.py`).


