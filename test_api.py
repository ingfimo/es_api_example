# The test implemented in the following focus mainly on
# checking that the API returns:
# 1. the correct status codes;
# 2. the correct error messages;
# 3. search result lists of the correct length.
# Other manual tests have been carried to verify
# the correctness of simple test searches.

from fastapi.testclient import TestClient
from apy import app, ES
from time import sleep

DOC_ID = None

DOC = {"title": "Test Document", "author": "Tester", "text": "This is a test document"}

UPDATED_DOC = {
    "title": "Updated Test Document",
    "author": "Tester",
    "text": "This is an updated version of the test document",
}

LARGE_DOC = {
    "title": "Large Test Document",
    "author": "Large Tester",
    "text": "\n".join(["This is a large test document." for i in range(100000)]),
}

tester = TestClient(app)


def test_getIndexSettings():
    """
    Tests for the correct status_code for the get '/settings' endpoint.
    """
    resp = tester.get("/settings")
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."


def test_getIndexMappings():
    """
    Tests for the correct status_code for the get '/mappings' endpoint.
    """
    resp = tester.get("/mappings")
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."


def test_getAll():
    """
    Tests for the correct status_code for get '/retrieve/all' endpoint when the 
    index is empty.
    """
    resp = tester.get("/retrieve/all")
    assert resp.status_code == 204, f"'status_code' != 204 ({resp.status_code})."


def test_addIllegal():
    """
    Tests for the correct status_code for adding an illegal document.
    """
    resp = tester.put("/new", json={"title": "Illegal Document", "author": "Outlaw"})
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "A document must have a text."


def test_indexDoc_new():
    """
    Adds a document, tests for the correct status_code, and check that exists in the index.
    Saves its id in the global variable DOC_ID to be used later in other tests.
    """
    global DOC, DOC_ID
    resp = tester.put("/new", json=DOC)
    sleep(5)
    assert resp.status_code == 201, f"'status_code' != 201 ({resp.status_code})"
    DOC_ID = resp.json()["_id"]
    assert ES.docExists(
        DOC_ID
    ), f"indexed document with id {resp.json()}resp.does not exists."


def test_getDoc():
    """
    Retrieves the added document, tests for the correct status_code, 
    and checks for correspondence.
    """
    global DOC_ID, DOC
    resp = tester.get(f"/retrieve/id/{DOC_ID}")
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    assert resp.json()["_id"] == DOC_ID, "'looked up id and retrieved id are different."
    doc = resp.json()["_source"]
    assert doc["title"] == DOC["title"], "'title' does not correspond."
    assert doc["author"] == DOC["author"], "'author' does not correspond."
    assert doc["text"] == DOC["text"], "'text' does not correspond."


def test_updateNonExisting():
    """
    Updates a non-existing document and tests for the correct status_code.
    """
    global UPDATED_DOC
    doc = UPDATED_DOC.copy()
    doc["id"] = "non-existing"
    resp = tester.put("/update", json=doc)
    assert resp.status_code == 404, f"'status_code' != 404 ({resp.status_code})."
    assert resp.json()["detail"] == f"document wit id {doc['id']} not found."


def test_updateExisting():
    """
    Updates an existing document and tests for the correct status_code.
    """
    global DOC_ID, UPDATED_DOC
    doc = UPDATED_DOC.copy()
    doc["id"] = DOC_ID
    resp = tester.put("/update", json=doc)
    sleep(5)
    assert resp.status_code == 202, f"'status_code' != 404 ({resp.status_code})."


def test_updatedDocument():
    """
    Checks the update previously performed.
    Tests for the correct status_code and correspondence between the document fields.
    """
    global DOC_ID, UPDATED_DOC
    resp = tester.get(f"/retrieve/id/{DOC_ID}")
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    assert resp.json()["_id"] == DOC_ID, "'looked up id and retrieved id are different."
    doc = resp.json()["_source"]
    assert doc["title"] == UPDATED_DOC["title"], "'title' does not correspond."
    assert doc["author"] == UPDATED_DOC["author"], "'author' does not correspond."
    assert doc["text"] == UPDATED_DOC["text"], "'text' does not correspond."


def test_simpleSearch_empty():
    """
    Tests for the correct status_code for an empty search result list.
    """
    resp = tester.post("/search/simple", json={"pattern": "zzzz"})
    assert resp.status_code == 204, f"'status_code' != 204 ({resp.status_code})."


def test_simpleSearch():
    """
    Tests for the correct status_code for a simple search returning some documents.
    """
    resp = tester.post("/search/simple", json={"pattern": "test"})
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."


def test_simpleSearch_illegal_k():
    """
    Tests for the correct status_code and error message for illegal values of k.
    """
    resp = tester.post("/search/simple", json={"pattern": "test", "k": 0})
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'k' must be higher than 0."


def test_simpleSearch_illegal_pattern():
    """
    Tests for the correct status_code and error message for illegal values of pattern.
    """
    resp = tester.post("/search/simple", json={"pattern": None, "k": 10})
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'pattern' cannot be None."


def test_simpleSearchFuzzy():
    """
    Tests for the correct status_code for a simple fuzzy search returning some documents.
    """
    resp = tester.post("/search/simple/fuzzy", json={"pattern": "tst"})
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."


def test_simpleSearchFuzzy_illegal_k():
    """
    Tests for the correct status_code and error message for illegal values of pattern.
    """
    resp = tester.post("/search/simple/fuzzy", json={"pattern": "tst", "k": -1})
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'k' must be higher than 0."


def test_simpleSearchFuzzy_illegal_pattern():
    """
    Tests for the correct status_code and error message for illegal values of pattern.
    """
    resp = tester.post("/search/simple/fuzzy", json={"pattern": "", "k": 5})
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'pattern' must contain at least one character."


def test_weightedSearch():
    """
    Tests for the correct status_code for a weighted search returning some documents.
    """
    resp = tester.post(
        "/search/weighted",
        json={
            "pattern": "test",
            "k": 4,
            "boost_title": 2,
            "boost_author": 1,
            "boost_text": 1,
        },
    )
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."


def test_weightedSearch_illegal_boosting_author():
    """
    Tests for the correct status_code and error message for illegal values of boosting_author.
    """
    resp = tester.post(
        "/search/weighted",
        json={
            "pattern": "test",
            "k": 11,
            "boost_title": 2,
            "boost_author": -1,
            "boost_text": 1,
        },
    )
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'boost_author' must be higher then or equal to 0."


def test_weightedSearch_illegal_boosting_title():
    """
    Tests for the correct status_code and error message for illegal values of boosting_title.
    """
    resp = tester.post(
        "/search/weighted",
        json={
            "pattern": "test",
            "k": 5,
            "boost_title": -2,
            "boost_author": 1,
            "boost_text": 1,
        },
    )
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'boost_title' must be higher then or equal to 0."


def test_weightedSearchFuzzy():
    """
    Tests for the correct status_code for a weighted fuzzy search returning some documents.
    """
    resp = tester.post(
        "/search/weighted/fuzzy",
        json={
            "pattern": "tost",
            "k": 6,
            "boost_title": 2,
            "boost_author": 1,
            "boost_text": 1,
        },
    )
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."


def test_weightedSearchFuzzy_illegal_boosting_text():
    """
    Tests for the correct status_code and error message for illegal values of boosting_text.
    """
    resp = tester.post(
        "/search/weighted/fuzzy",
        json={
            "pattern": "tost",
            "k": 3,
            "boost_title": 2,
            "boost_author": 1,
            "boost_text": -1,
        },
    )
    assert resp.status_code == 400, f"'status_code' != 400 ({resp.status_code})."
    assert resp.json()["detail"] == "'boost_text' must be higher then or equal to 0."


def test_addSomeDocs():
    """
    Adds some dummy documents to the index
    """
    for i in range(1, 10):
        resp = tester.put("/new", json=DOC)
        sleep(1)
        assert resp.status_code == 201, f"'status_code' != 201 ({resp.status_code})"
    for i in range(1, 10):
        resp = tester.put("/new", json=DOC)
        sleep(1)
        assert resp.status_code == 201, f"'status_code' != 201 ({resp.status_code})"


def test_simpleSearch_k():
    """
    Tests for correct length of the result list for a simple search.
    """
    k = 7
    resp = tester.post("/search/simple", json={"pattern": "test", "k": k})
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    hits = resp.json()["hits"]["hits"]
    assert len(hits) == k, "result list length i wrong."


def test_simpleSearchFuzzy_k():
    """
    Tests for correct length of the result list for a simple fuzzy search.
    """
    k = 2
    resp = tester.post("/search/simple/fuzzy", json={"pattern": "tesr", "k": k})
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    hits = resp.json()["hits"]["hits"]
    assert len(hits) == k, "result list length i wrong."


def test_weightedSearch_k():
    """
    Tests for correct length of the result list for a weighted search.
    """
    k = 11
    resp = tester.post(
        "/search/weighted",
        json={
            "pattern": "test",
            "k": k,
            "boost_title": 2,
            "boost_author": 1,
            "boost_text": 1,
        },
    )
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    hits = resp.json()["hits"]["hits"]
    assert len(hits) == k, "result list length i wrong."


def test_weightedSearchFuzzy_k():
    """
    Tests for correct length of the result list for a weighted fuzzy search.
    """
    k = 17
    resp = tester.post(
        "/search/weighted/fuzzy",
        json={
            "pattern": "tedt",
            "k": k,
            "boost_title": 2,
            "boost_author": 1,
            "boost_text": 1,
        },
    )
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    hits = resp.json()["hits"]["hits"]
    assert len(hits) == k, "result list length i wrong."


def test_indexRetrieveLarge():
    """
    Tests ingestion and retrieval of large document
    """
    global LARGE_DOC
    resp = tester.put("/new", json=LARGE_DOC)
    assert resp.status_code == 201, f"'status_code' != 201 ({resp.status_code})"
    doc_id = resp.json()["_id"]
    assert ES.docExists(
        doc_id
    ), f"indexed document with id {resp.json()}resp.does not exists."
    resp = tester.get(f"/retrieve/id/{doc_id}")
    assert resp.status_code == 200, f"'status_code' != 200 ({resp.status_code})."
    assert resp.json()["_id"] == doc_id, "'looked up id and retrieved id are different."
    doc = resp.json()["_source"]
    assert doc["title"] == LARGE_DOC["title"], "'title' does not correspond."
    assert doc["author"] == LARGE_DOC["author"], "'author' does not correspond."
